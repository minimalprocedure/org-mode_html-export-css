# Emacs org-mode HTML css

**org.css**

* base org stylesheets for html export

**org-fixed.css**

* org stylesheets for html export with sidebar (TOC)

**org-toggle.css**

* org stylesheets for html export with toggled sidebar (TOC).

**sidebar-toggle.js**

* Companion _org-toggle.css_ file for button sidebar.
* Add `#+HTML_HEAD: <script type='text/javascript' src='sidebar-toggle.js'></script>` to org source.

## Credits:

http://necolas.github.io/normalize.css/ for normalize.css

## Licence:

**The MIT License (MIT)**

Copyright (c) 2015 Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.com>  

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
